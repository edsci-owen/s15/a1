console.log('Hello, World')
// Details
const details = {
    fName: "Mark",
    lName: "Owen",
    age: 45,
    hobbies : [
        "drawing", "hiking", "exercise"
    ] ,
    workAddress: {
        housenumber: "21st",
        street: "California",
        city: "New Mexico",
        state: "Texas",
    }
}
const work = Object.values(details.workAddress);
console.log("My First Name is " + details.fName)
console.log("My Last Name is " + details.lName)
console.log(`Yes, I am ${details.fName} ${details.lName}.`)
console.log("I am " + details.age + " years old.")
console.log(`My hobbies are ${details.hobbies.join(', ')}.`);
console.log("I work at " + work.join(", ") + ".");